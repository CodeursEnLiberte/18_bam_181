# Réponse dinsic
Développement et le design de services publics numériques en mode agile

## RC
Le niveau technique de l’offre est jugé suffisant dès l’obtention d’une note relative au critère technique supérieure ou égale à 32,5 points, soit la moitié des points de pourcentage du critère de valeur technique.

Une clause obligatoire d’exécution d’insertion par l’activité économique.
>  Pour l’exécution de l’accord-cadre, l’entreprise attributaire devra réaliser une action d’insertion qui permette l’accès ou le retour à l’emploi de personnes rencontrant des difficultés sociales ou professionnelles particulières
**1 heure d’insertion pour chaque tranche de 2000 € hors taxe facturés**

Les publics concernés
* les demandeurs d’emploi de longue durée (plus de 12 mois d’inscription au chômage)
* les allocataires du RSA (Revenu de Solidarité Active) ou leurs ayants droit
* les allocataires de l’AAH (Allocation Adulte Handicapé), de l’ASS (Allocation de Solidarité Spécifique), de l’AV (Allocation Veuvage)
* Les personnes percevant une pension d’invalidité
* les publics reconnus travailleurs handicapés, au sens de l’article L 5212-13 du Code du travail, fixant la liste des bénéficiaires de l’obligation d’emploi
* les jeunes de moins de 26 ans, de niveau infra 5, c’est-à-dire de niveau inférieur au CAP/BEP, et sortis du système scolaire depuis au moins 6 mois, les jeunes en suivi renforcé de type PACEA, ANI Jeunes, SMA, en sortie de dispositif Garantie Jeunes ou sous contrat EPIDE, dans un parcours de l’Ecole de la Deuxième Chance (E2C)
* les personnes prises en charge dans le dispositif IAE (Insertion par l’Activité Économique), c’est-à-dire les personnes mises à disposition par une Association Intermédiaire (AI) ou par une Entreprise de Travail Temporaire d’Insertion (ETTI), ainsi que des salariés d’une Entreprise d’Insertion (EI), d’un Atelier et Chantier d’Insertion (ACI), ou encore des Régies de quartier agrées, les personnes en sortie d’une SIAE, ainsi que les personnes prises en charge dans des dispositifs particuliers, par exemple « Défense 2ème chance »
* les personnes employées dans les GEIQ (Groupements d’Employeurs pour l’Insertion et la Qualification) et dans les associations poursuivant le même objet
* les personnes placées sous mains de justice employées en régie, dans le cadre du service de l’emploi pénitentiaire / régie des établissements pénitentiaires (SEP / RIEP) ou affectées à un emploi auprès d’un concessionnaire de l’administration pénitentiaire


### Candidature
- La lettre de candidature (formulaire DC1) dûment complétée ;
- La déclaration du candidat (formulaire DC2) dûment complétée ;
- Le candidat présente également les principales références vérifiables de sa société dans des domaines similaires à ceux de la présente procédure. Les références concernent les trois dernières années, les montants, la date ainsi que les destinataires publics ou privés doivent être précisés. Point de contact (si possible) : NOM, Numéro de téléphone ou mail.

#### Valeur technique 65 %
* Compréhension des enjeux et des besoins             5 %
* Compétences et capacité à travailler en mode agile  25 %
* Composition et organisation des équipes             30 %
* Transfert de compétences                            5 %

#### Prix 35 %
* Sous-critère prix n°1-Lancement d'une Start-up d'Etat   10 %
* Sous-critère prix n°2 - Lancement et croissance d'une Start-up d'Etat 15 %
* Sous-critère prix n°3 - Accompagnement d'une Start-up d'Etat performante du portefeuille  10 %

## CCTP
* à assurer la qualité, la modernité et l’efficacité du système d’information de l’État ;
* à accompagner le développement des nouveaux services publics numériques ;
* et à soutenir la transformation des administrations.
### Objet
* accompagner la croissance de ce dispositif en contribuant à la création et à la diffusion des services développés ainsi qu’à l’amélioration continue du pilotage du dispositif ;
* diffuser ces méthodes à tout autre projet numérique de la DINSIC

### Start-up d'état
* une phase d’investigation, où on s’assure d’avoir un problème qui vaut la peine d’être résolu par un service public numérique ;
* une phase de construction, où on s’assure de créer un service public numérique qui répond au problème ;
* une phase de consolidation, où on découvre la manière de diffuser le service en élargissant son public cible et son offre fonctionnelle ;
* une phase de passation, où on définit avec l’administration porteuse des conditions de passage à l’échelle du service.

#### UO Devops – Développement et exploitation d’un service numérique
* conception et l’amélioration continue d’un service numérique sur l’ensemble de la stack
* l'interaction régulière avec des utilisateurs afin de comprendre au mieux leurs besoins
* la conception et la mise en place systématiques de tests automatisés pour assurer la qualité et la non-régression du service
* le développement et la mise à jour de scripts de déploiements automatisés
* l’outillage de la croissance du service, notamment par le désendettement technique de l’application et la prise en compte itérative des mesures de sécurité nécessaires
* la rédaction et la mise à jour de la documentation nécessaire.

Cette prestation dure environ trois mois

#### UO Design - Conception de l’expérience utilisateur
* la recherche utilisateurs ;
* la conception d’expériences utilisateurs en fonction de l’observation de leurs besoins ;
* la conception d’interfaces qui reflètent ces apprentissages et optimisent le tunnel de conversion du service.

Cette prestation dure environ un mois

#### UO Déploiement - Business development : mise en oeuvre de la stratégie de déploiement d’un produit
* la mise en oeuvre d’une stratégie de croissance du service, s’appuyant sur l’identification d’early adopters et des leviers organiques de la diffusion du service
* le suivi continu des métriques d’usage
* l’animation d’une communauté de partenaires engagés dans le déploiement du service
* l’amélioration continue du support utilisateur
* toute activité visant à améliorer le tunnel de conversion du service
Cette prestation dure environ 3 mois

#### UO Hébergement : gestion de l’environnement technique d’un service numérique
Cette prestation recouvre la mise en place de l’environnement technique (plateforme d’hébergement), la maintenance et l’intégration des nouvelles fonctionnalités nécessaires au développement du service public numérique.

#### UO Réversibilité : transfert portant sur l’ensemble des projets de Start-up à la fin du marché
L’UO de réversibilité est déclenchée sur décision de l’administration.
Cette phase ne doit pas durer plus de deux mois.

#### Prestations
Les prestations décrites au CCTP sont décomposées en unités d’oeuvre, une unité d’oeuvre répond à un besoin précis et couvre un ensemble d’actions réalisées par le titulaire permettant de produire les livrables identifiés dans les délais convenus.
Chaque unité d’oeuvre est précisée par une charge de base d’exécution forfaitaire avec obligation de résultats. Une prestation formalisée dans un bon de commande peut faire l’objet de plusieurs unités d’oeuvre en fonction du calibrage des résultats attendus. Ce calibrage est convenu avec le titulaire pour permettre l’établissement du bon de commande.

En réponse à une expression de besoin transmise par la DINSIC au titulaire, celui-ci formalise une proposition d’intervention (qui sera annexée au bon de commande qu’émettra l’Administration) contenant au minimum :
* une reformulation de sa compréhension du besoin ;
* une description de son intervention ;
* la liste des livrables qu’il s’engage à produire tels que prévus dans la description des unités d’oeuvre;
* les délais de remise de ces livrables ;
* la référence et le nombre d’unités d’oeuvre sollicitées pour répondre au besoin ;
* les intervenants du titulaire mobilisés et les charges estimatives d’intervention ;
* les attendus de l’Administration pour la bonne exécution de la prestation ;
* la signature d’une des personnes habilitées à engager le titulaire.

## CCAP

Envoyer ses factures par raccordement direct à la solution mutualisée ou à partir d'un système tiers :
* en utilisant des web services (en mode API - Application Programming Interface) : Chorus Pro offre l'ensemble de ses fonctionnalités sous forme de services intégrés dans un portail tiers (API/web service).

https://communaute.chorus-pro.gouv.fr/deposer-flux-facture/
````
{
  "idUtilisateurCourant" : 331,
  "fichierFlux" : "Fichier encodé en base 64",
  "nomFichier" : "24372_GDP-CU-13-SXP-01-TST_01 DeposerFluxFacture - IN_DP_E1_UBL_INVOICE - XML avec signature.xml",
  "syntaxeFlux" : "IN_DP_E1_UBL_INVOICE",
  "avecSignature" : true
}
````

L'émetteur de facture s'identifie via les API, et accède à l'ensemble des services de Chorus Pro comme par exemple le dépôt ou saisie de factures, le suivi du traitement des factures, l'adjonction et téléchargement de pièces complémentaires, etc.

### Contacts financiers
* Services du Contrôle budgétaire et comptable ministériel auprès des services du Premier ministre
Service facturier - DSAF
20, avenue de Ségur – 75007 PARIS

* L'ordonnateur des paiements est :
Monsieur le représentant de la Direction des services administratifs et financiers par délégation du Premier ministre
20, avenue de Ségur – 75007 PARIS

* Comptable :
Contrôleur budgétaire et comptable ministériel auprès des services du Premier ministre
Département comptable ministériel
20, avenue de Ségur – 75007 PARIS

Les sommes dues sont payées conformément aux dispositions du titre IV de la loi n°2013-100 du 28 janvier 2013 portant diverses dispositions d’adaptation de la législation au droit de l’Union européenne en matière économique et financière et du décret n°2013-269 du 29 mars 2013 relatif à la lutte contre les retards de paiements dans les contrats de la commande publique.

**Le délai global de paiement ne peut excéder 30 jours.**

Les opérations de vérification des prestations ont pour but de s’assurer que les productions réalisées sont conformes aux prescriptions fixées dans le présent CCAP et dans le CCTP.
A la fin de chaque prestation, les livrables produits par le titulaire sont adressés à la personne publique par voie électronique.

A compter de la remise des livrables, le représentant du pouvoir adjudicateur se réserve un délai d’**un mois maximum pour prononcer l'admission des livrables**.

### Avances
Sauf refus exprimé par le titulaire dans l’acte d’engagement, pour chaque bon de commande d’un montant supérieur à 50 000 € HT et d’une durée d’exécution supérieure à deux mois mais inférieure ou égale à douze mois, une avance de 5 % du montant de ce bon de commande est versée au titulaire, conformément aux articles 110 à 113 du décret n°2016-360 du 25 mars 2016 relatif aux marchés publics.

Son remboursement s’effectue selon les modalités prévues par l'article 111 du décret n°2016-360 du 25 mars 2016 relatif aux marchés publics.

## Réponse
1. Exposez en une demi-page maximum votre compréhension des enjeux des start-up d’Etat.
2. Décrivez votre conception et vos modalités de travail en mode agile.
Présentez deux démarches de conception et de développement d’un service dans une approche agile ou Lean Start Up (2 pages maximum/service).
Pour chaque projet, indiquez :
- le problème que le service cherchait à résoudre
- les méthodes et outils d’écoute des usagers
- la répartition des rôles et des compétences de l’ensemble des personnes impliquées
- les modalités d’animation de l’amélioration continue de l’équipe
- les méthodes et outils de priorisation du travail en équipe
- les langages de programmation utilisés
- les dépôts de code associés au service
- les éventuelles contributions reçues sur le code publié
- les résultats obtenus
- les difficultés rencontrées
- les apprentissages
- les facteurs clés de réussite

3. Présentez les différents profils que vous mobiliserez pour constituer les équipes des différentes phases.
L’offre doit comporter les CV anonymisés des différents profils susceptibles d’être mobilisés. Les CV doivent être suffisamment détaillés pour permettre de juger de l’expertise des intervenants.

4. Décrivez les actions, méthodes et outils que vous envisagez de mettre en place pour garantir le transfert de compétences vers l’administration, au fil de la prestation ainsi qu’au moment de la phase de réversibilité.
