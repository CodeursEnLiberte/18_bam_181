# Réponse à l’appel à projet de la DINSIC

Un groupe de coopératives et d’indépendants envisage de répondre au nouvel appel à projet de la DINSIC, qui sert à principalement à employer des développeurs pour beta.gouv et etalab.

- Référence de consultation : 18_BAM_181
- https://www.marches-publics.gouv.fr/app.php/consultation/373964
- Date de dépot: **7 juin**
- 3 attributaires (donc 3 bénéficiaires du marché).
- Il s’agit de faire une candidature groupée (pour être un des attributaires).