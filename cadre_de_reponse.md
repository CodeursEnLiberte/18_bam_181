_Le cadre de réponse suivant correspond au cœur du mémoire technique attendu. Le candidat doit, à minima, répondre aux questions suivantes. Il peut également, s’il le souhaite, enrichir son offre de documents annexes. L’ensemble de la proposition ne doit pas excéder 20 pages (et au maximum 30 pages en incluant les annexes)._


_Pour répondre, en dehors des papiers administratif, il faudra construire une réponse à :_

# Réponse à l'appel à projet du groupement


## Compréhension des enjeux et des besoins (5%)
_Exposez en une demi-page maximum votre compréhension des enjeux des start-up d’Etat._


## Compétences et capacité à travailler en mode agile (25%)
_Décrivez votre conception et vos modalités de travail en mode agile._
_Présentez deux démarches de conception et de développement d’un service dans une approche agile ou Lean Start Up (2 pages maximum/service)._

Pour chaque projet, indiquez :
- le problème que le service cherchait à résoudre
- les méthodes et outils d’écoute des usagers
- la répartition des rôles et des compétences de l’ensemble des personnes impliquées
- les modalités d’animation de l’amélioration continue de l’équipe
- les méthodes et outils de priorisation du travail en équipe
- les langages de programmation utilisés
- les dépôts de code associés au service
- les éventuelles contributions reçues sur le code publié
- les résultats obtenus
- les difficultés rencontrées
- les apprentissages
- les facteurs clés de réussite

Des liens vers les codes sources des services en ligne développés permettront de juger de l’expertise en matière de développement. Des exemples de livrables produits dans le cadre de ces activités seront à fournir.

## Composition et organisation des équipes (30%)
Présentez les différents profils que vous mobiliserez pour constituer les équipes des différentes phases. L’offre doit comporter les CV anonymisés des différents profils susceptibles d’être mobilisés. Les CV doivent être suffisamment détaillés pour permettre de juger de l’expertise des intervenants. Les contributions des personnes mobilisées à des projets opensource (profil Github, gitlab...) seront à fournir.

Les profils présentés devront comprendre des profils seniors (ex-CTO et Tech lead), autonomes, compétents techniquement et en capacité d’encadrer des équipes de développeurs. Le témoignage de sous traitants, experts du numérique (devops, design, tech lead, CTO...), ayant collaboré ou étant intervenu en sous-traitant de l’attributaire seront à fournir afin de démontrer la capacité du prestataire à mobiliser un réseau de partenaires, acteurs de l’économie numérique.

## Transfert de compétences (5%)
Décrivez les actions, méthodes et outils que vous envisagez de mettre en place pour garantir le transfert de compétences vers l’administration, au fil de la prestation ainsi qu’au moment de la phase de réversibilité. Le candidat devra également présenter la manière dont il prévoit en début de marché de s’organiser en vue d’assurer dans les meilleures conditions possibles la prise en charge des projets en cours.

